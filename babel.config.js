
module.exports = {
  babelrcRoots: ['.'],
  presets: [
    ['@babel/preset-env', {
      corejs: 3,
      modules: false,
      useBuiltIns: 'usage',
    }],
  ],
  plugins: [
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-object-rest-spread',
    '@babel/plugin-proposal-optional-chaining',
    '@babel/plugin-syntax-dynamic-import',
    '@babel/plugin-transform-modules-commonjs',
    ['module-resolver', { root: ['./src'] }],
  ],
}
