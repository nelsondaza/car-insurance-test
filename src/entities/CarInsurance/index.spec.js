
import spies from 'chai-spies'
import { expect, spy, use } from 'chai'

import m from 'mocks'

import Subject from './index'

use(spies)

describe('entities::CarInsurance', () => {
  it('has an empty list of products by default', () => {
    const target = new Subject()
    expect(target.products.length).equal(0)
  })

  it('calls updatePrice for each product', () => {
    const product = m.product()
    spy.on(product, 'updatePrice')

    const target = new Subject([product, product])
    expect(product.updatePrice).to.not.have.been.called()
    target.updatePrice()
    expect(product.updatePrice).to.have.been.called.exactly(2)
  })
})
