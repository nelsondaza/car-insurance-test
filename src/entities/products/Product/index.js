
export default class Product {
  maxPrice = 50

  minPrice = 0

  priceChange = -1

  constructor(name, sellIn, price) {
    this.name = name
    this.sellIn = sellIn
    this.price = price
  }

  getPriceAdjustment = () => (this.sellIn > 0 ? this.priceChange : this.priceChange * 2)

  updatePrice() {
    this.price += this.getPriceAdjustment()
    this.sellIn -= 1

    if (this.price > this.maxPrice) {
      this.price = this.maxPrice
    }
    if (this.price < this.minPrice) {
      this.price = this.minPrice
    }
  }
}
