
import { expect } from 'chai'

import Subject from './index'

describe('entities::products::Product', () => {
  const expectedValuesOnUpdate = [
    { target: new Subject('NAME', 0, 2), sellIn: -1, price: 0 },
    { target: new Subject('NAME', 4, 6), sellIn: 3, price: 5 },
    { target: new Subject('NAME', -6, 0), sellIn: -7, price: 0 },
  ]
  expectedValuesOnUpdate.forEach(({ target, sellIn, price }) => {
    describe(`after day ${target.sellIn} with price ${target.price}`, () => {
      target.updatePrice()

      it(`sellIn should be ${sellIn}`, () => {
        expect(target.sellIn).equal(sellIn)
      })

      it(`price should be ${price}`, () => {
        expect(target.price).equal(price)
      })
    })
  })
})
