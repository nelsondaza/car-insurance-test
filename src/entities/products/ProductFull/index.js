
import Product from '../Product'

export default class ProductFull extends Product {
  priceChange = 1

  constructor(sellIn, price) {
    super('Full Coverage', sellIn, price)
  }
}
