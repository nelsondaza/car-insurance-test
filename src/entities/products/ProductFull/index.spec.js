
import { expect } from 'chai'

import Subject from './index'

describe('entities::products::ProductFull', () => {
  it('has "Full Coverage" as name ', () => {
    expect((new Subject(2, 2)).name).equal('Full Coverage')
  })

  const expectedValuesOnUpdate = [
    { target: new Subject(2, 0), sellIn: 1, price: 1 },
    { target: new Subject(1, 1), sellIn: 0, price: 2 },
    { target: new Subject(0, 2), sellIn: -1, price: 4 },
    { target: new Subject(-5, 12), sellIn: -6, price: 14 },
    { target: new Subject(2, 50), sellIn: 1, price: 50 },
    { target: new Subject(-2, 50), sellIn: -3, price: 50 },
    { target: new Subject(0, 80), sellIn: -1, price: 50 },
  ]
  expectedValuesOnUpdate.forEach(({ target, sellIn, price }) => {
    describe(`after day ${target.sellIn} with price ${target.price}`, () => {
      target.updatePrice()

      it(`sellIn should be ${sellIn}`, () => {
        expect(target.sellIn).equal(sellIn)
      })

      it(`price should be ${price}`, () => {
        expect(target.price).equal(price)
      })
    })
  })
})
