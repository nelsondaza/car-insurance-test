
import Product from '../Product'

export default class ProductLow extends Product {
  constructor(sellIn, price) {
    super('Low Coverage', sellIn, price)
  }
}
