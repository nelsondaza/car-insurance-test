
import Product from '../Product'

export default class ProductMedium extends Product {
  constructor(sellIn, price) {
    super('Medium Coverage', sellIn, price)
  }
}
