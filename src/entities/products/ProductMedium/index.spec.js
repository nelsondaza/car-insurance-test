
import { expect } from 'chai'

import Subject from './index'

describe('entities::products::ProductMedium', () => {
  it('has "Medium Coverage" as name ', () => {
    expect((new Subject(2, 2)).name).equal('Medium Coverage')
  })

  const expectedValuesOnUpdate = [
    { target: new Subject(2, 12), sellIn: 1, price: 11 },
    { target: new Subject(0, 10), sellIn: -1, price: 8 },
    { target: new Subject(-5, 0), sellIn: -6, price: 0 },
  ]
  expectedValuesOnUpdate.forEach(({ target, sellIn, price }) => {
    describe(`after day ${target.sellIn} with price ${target.price}`, () => {
      target.updatePrice()

      it(`sellIn should be ${sellIn}`, () => {
        expect(target.sellIn).equal(sellIn)
      })

      it(`price should be ${price}`, () => {
        expect(target.price).equal(price)
      })
    })
  })
})
