
import Product from '../Product'

export default class ProductMega extends Product {
  maxPrice = 80

  minPrice = 80

  priceChange = 0

  constructor(sellIn) {
    super('Mega Coverage', sellIn, 80)
  }

  updatePrice() {
    this.sellIn += 1

    super.updatePrice()
  }
}
