
import { expect } from 'chai'

import Subject from './index'

describe('entities::products::ProductMega', () => {
  it('has "Mega Coverage" as name ', () => {
    expect((new Subject(2, 2)).name).equal('Mega Coverage')
  })

  const expectedValuesOnUpdate = [
    { target: new Subject(15, 20), sellIn: 15, price: 80 },
    { target: new Subject(4, 50), sellIn: 4, price: 80 },
    { target: new Subject(2), sellIn: 2, price: 80 },
    { target: new Subject(10), sellIn: 10, price: 80 },
    { target: new Subject(6, 33), sellIn: 6, price: 80 },
    { target: new Subject(3, 33), sellIn: 3, price: 80 },
    { target: new Subject(-23, 33), sellIn: -23, price: 80 },
  ]
  expectedValuesOnUpdate.forEach(({ target, sellIn, price }) => {
    describe(`after day ${target.sellIn} with price ${target.price}`, () => {
      target.updatePrice()

      it(`sellIn should be ${sellIn}`, () => {
        expect(target.sellIn).equal(sellIn)
      })

      it(`price should be ${price}`, () => {
        expect(target.price).equal(price)
      })
    })
  })
})
