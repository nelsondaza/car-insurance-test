
import Product from '../Product'

export default class ProductSpecial extends Product {
  priceChange = 1

  constructor(sellIn, price) {
    super('Special Full Coverage', sellIn, price)
  }

  getPriceAdjustment = () => {
    if (this.sellIn <= 5) {
      return this.priceChange * 3
    }
    return this.sellIn > 10 ? this.priceChange : this.priceChange * 2
  }

  updatePrice() {
    if (this.sellIn <= 0) {
      this.price = 0
      this.priceChange = 0
    }

    super.updatePrice()
  }
}
