
import { expect } from 'chai'

import Subject from './index'

describe('entities::products::ProductSpecial', () => {
  it('has "Special Full Coverage" as name ', () => {
    expect((new Subject(2, 2)).name).equal('Special Full Coverage')
  })

  const expectedValuesOnUpdate = [
    { target: new Subject(15, 20), sellIn: 14, price: 21 },
    { target: new Subject(4, 50), sellIn: 3, price: 50 },
    { target: new Subject(0, 50), sellIn: -1, price: 0 },
    { target: new Subject(-5, 0), sellIn: -6, price: 0 },
    { target: new Subject(2, 80), sellIn: 1, price: 50 },
    { target: new Subject(1, 80), sellIn: 0, price: 50 },
    { target: new Subject(0, 80), sellIn: -1, price: 0 },
    { target: new Subject(10, 25), sellIn: 9, price: 27 },
    { target: new Subject(6, 33), sellIn: 5, price: 35 },
    { target: new Subject(3, 33), sellIn: 2, price: 36 },
  ]
  expectedValuesOnUpdate.forEach(({ target, sellIn, price }) => {
    describe(`after day ${target.sellIn} with price ${target.price}`, () => {
      target.updatePrice()

      it(`sellIn should be ${sellIn}`, () => {
        expect(target.sellIn).equal(sellIn)
      })

      it(`price should be ${price}`, () => {
        expect(target.price).equal(price)
      })
    })
  })
})
