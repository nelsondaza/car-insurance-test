
import Product from '../Product'

export default class ProductSuper extends Product {
  priceChange = -2

  constructor(sellIn, price) {
    super('Super Sale', sellIn, price)
  }
}
