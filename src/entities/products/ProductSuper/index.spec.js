
import { expect } from 'chai'

import Subject from './index'

describe('entities::products::ProductSuper', () => {
  it('has "Super Sale" as name ', () => {
    expect((new Subject(2, 2)).name).equal('Super Sale')
  })

  const expectedValuesOnUpdate = [
    { target: new Subject(2, 4), sellIn: 1, price: 2 },
    { target: new Subject(0, 10), sellIn: -1, price: 6 },
  ]
  expectedValuesOnUpdate.forEach(({ target, sellIn, price }) => {
    describe(`after day ${target.sellIn} with price ${target.price}`, () => {
      target.updatePrice()

      it(`sellIn should be ${sellIn}`, () => {
        expect(target.sellIn).equal(sellIn)
      })

      it(`price should be ${price}`, () => {
        expect(target.price).equal(price)
      })
    })
  })
})
