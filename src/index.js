/* eslint-disable no-console */

import CarInsurance from 'entities/CarInsurance'
import {
  ProductFull,
  ProductLow,
  ProductMedium,
  ProductMega,
  ProductSpecial,
  ProductSuper,
} from 'entities/products'

const productsAtDayZero = [
  new ProductMedium(10, 20),
  new ProductFull(2, 0),
  new ProductLow(5, 7),
  new ProductMega(0),
  new ProductMega(-1),
  new ProductSpecial(15, 20),
  new ProductSpecial(10, 49),
  new ProductSpecial(5, 49),
  new ProductSuper(3, 6),
]

const carInsurance = new CarInsurance(productsAtDayZero)
const productPrinter = product => console.log(`${product.name}, ${product.sellIn}, ${product.price}`)

console.log('OMGHAI!')
console.log('-------- day 0 --------')
console.log('name, sellIn, price')
productsAtDayZero.forEach(productPrinter)
console.log('')

for (let i = 1; i <= 30; i += 1) {
  console.log(`-------- day ${i} --------`)
  console.log('name, sellIn, price')
  carInsurance.updatePrice().forEach(productPrinter)
  console.log('')
}
