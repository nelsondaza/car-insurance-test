
import Product from 'entities/products/Product'

export default (data = {}) => (new Product(
  data.name || 'name',
  data.sellIn || 2,
  data.price || 50,
))
