
import { expect } from 'chai'

import Subject from './product'

describe('mock::Product', () => {
  it('can be created with no parameters', () => {
    const target = new Subject()
    expect(target.name).equal('name')
  })

  it('can be created with overriding values', () => {
    const target = new Subject({ name: 'new name' })
    expect(target.name).equal('new name')
  })
})
